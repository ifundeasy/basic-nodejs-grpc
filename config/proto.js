const protoLoaderOpts = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
  arrays: true,
  objects: true,
  json: true,
}

module.exports = {
  protoLoaderOpts
}