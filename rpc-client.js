const { parsed: env } = require('dotenv').config();
const grpc = require("@grpc/grpc-js");
const { loadSync } = require("@grpc/proto-loader");
const { protoLoaderOpts } = require('./config/proto')
const bookDefinition = loadSync("./protos/book.proto", protoLoaderOpts);
const { Book } = grpc.loadPackageDefinition(bookDefinition);

const book = new Book(
  `${env.HOST}:${parseInt(env.PORT) + 1}`,
  grpc.credentials.createInsecure()
);

module.exports = {
  book
};
