const { parsed: env } = require('dotenv').config()
const http = require('http')
const { book } = require('./rpc-client')

const host = env.HOST
const port = env.PORT

const requestListener = function (req, res) {
  const url = req.url.split('/')
  const method = req.method

  if (url[0] == 'book')
    switch (method) {
      case 'GET':
        if (url.length > 1 && url[1]) {
          book.get({ id: url[1] }, (error, data) => {
            if (error) throw error
            res.end(data)
          })
        }
        book.getAll({}, (error, data) => {
          if (error) throw error
          res.end(data)
        })
        res.end()

        break
      case 'PUT':
        book.edit(
          {
            id: url[1],
            body: req.body.body,
            postImage: req.body.postImage,
            title: req.body.title,
          },
          (error, data) => {
            if (error) throw error
            res.end(data)
          }
        )

        break
      case 'DELETE':
        book.delete({ id: url[1] }, (error, data) => {
          if (error) throw error
          res.end({ msg: 'Successfully deleted a book.' })
        })

        break
      case 'POST':
        book.add(
          {
            body: req.body.body,
            postImage: req.body.postImage,
            title: req.body.title,
          },
          (error, data) => {
            if (error) throw error
            res.end({ data, msg: 'Successfully created a book.' })
          }
        )
        break
      default:
        res.end('')
        break
    }
}

const server = http.createServer(requestListener)
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`)
})
