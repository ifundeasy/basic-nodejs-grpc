const { book } = require("./rpc-client");

// get all book
book.getAll(null, function (error, res) {
  if (error) throw error;
  console.log(res);
});

// get book
book.get({ id: 1 }, (error, res) => {
  if (error) throw error;
  console.log(res);
});

// add a book
book.add(
  {
    title: "Title book 3",
    body: "Body content 3",
    postImage: "Image URL here",
  },
  (error, res) => {
    if (error) throw error;
    console.log("Successfully created a book.");
  }
);

// edit a book
book.edit(
  {
    id: 2,
    body: "Body content 2 edited.",
    postImage: "Image URL edited.",
    title: "Title for 2 edited.",
  },
  (error, res) => {
    if (error) throw error;
    console.log("Successfully edited a book.");
  }
);

// delete a book
book.delete(
  {
    id: 2,
  },
  (error, res) => {
    if (error) throw error;
    console.log("Successfully deleted a book.");
  }
);

// add a book
book.add(
  {
    title: "Title book 4",
    body: "Body content 4",
    postImage: "Image URL here",
  },
  (error, res) => {
    if (error) throw error;
    console.log("Successfully created a book.");
  }
);

// add a book
book.add(
  {
    title: "Title book 5",
    body: "Body content 5",
    postImage: "Image URL here",
  },
  (error, res) => {
    if (error) throw error;
    console.log("Successfully created a book.");
  }
);

// get all book
book.getAll(null, function (error, res) {
  if (error) throw error;
  console.log(res);
});