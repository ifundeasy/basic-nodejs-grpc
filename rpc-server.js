const { parsed: env } = require('dotenv').config();
const { loadSync } = require("@grpc/proto-loader");
const grpc = require("@grpc/grpc-js");

const { protoLoaderOpts } = require('./config/proto')
const bookDefinition = loadSync("./protos/book.proto", protoLoaderOpts);
const package = grpc.loadPackageDefinition(bookDefinition);
const server = new grpc.Server();

let Datas = [
  { id: "1", title: "Note 1", body: "Content 1", postImage: "Post image 1" },
  { id: "2", title: "Note 2", body: "Content 2", postImage: "Post image 2" },
];
let NextId = Datas.length + 1;

server.addService(package.Book.service, {
  getAll: function (_, callback) {
    callback(null, { items: Datas });
  },
  get: (_, callback) => {
    const dataId = _.request.id;
    const datas = Datas.find(({ id }) => dataId == id);
    callback(null, datas);
  },
  delete: (_, callback) => {
    const dataId = _.request.id;
    Datas = Datas.filter(({ id }) => id != dataId);
    callback(null, {});
  },
  edit: (_, callback) => {
    const dataId = _.request.id;
    const dataItem = Datas.find(({ id }) => dataId == id);
    dataItem.body = _.request.body;
    dataItem.postImage = _.request.postImage;
    dataItem.title = _.request.title;
    callback(null, dataItem);
  },
  add: (call, callback) => {
    let data = { ...call.request, id: NextId };
    Datas.push(data);
    callback(null, data);

    NextId++;
  },
});

server.bindAsync(
  `${env.HOST}:${parseInt(env.PORT)+1}`,
  grpc.ServerCredentials.createInsecure(),
  (error, port) => {
    console.log("Server at port:", port);
    server.start();
  }
);
